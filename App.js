import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import SongsList from './components/SongsList';
import SongDetail from './components/SongDetail';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Provider as PaperProvider} from 'react-native-paper';

import firebase from 'firebase';

const Stack = createStackNavigator();

export default function App() {

  return (
    <PaperProvider>
      <NavigationContainer>
        <Stack.Navigator>
          
              <Stack.Screen name="Judapp" component={SongsList} options={{headerShown: false}}/>
              <Stack.Screen name="SongDetail" component={SongDetail} options={({ route }) => ({
                title: route.params.name
              })}/>
        </Stack.Navigator>
        <StatusBar style="auto" />
      </NavigationContainer>
    </PaperProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
