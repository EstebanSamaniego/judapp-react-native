import {  StyleSheet } from 'react-native';

const styles =  StyleSheet.create({
    container: {
        padding: 20,
    },
    lyrics: { 
        width: 300,
        fontSize: 20,
    }
});

export default styles;