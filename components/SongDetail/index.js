import React, { Component, useState, useEffect } from 'react';
import { Text, View, Pressable, SafeAreaView, ScrollView, Dimensions } from 'react-native'
import { ScreenContainer } from 'react-native-screens';

import styles from './styles';
//import lyrics from './lyrics';
import db from '../../database/firebase';


const SongDetail = ({ route }) => {

    const [lyric, setLyric] = useState([]);
    const uuid = route.params.uuid;

    function readFunction(){
        db.ref('Lyric').once('value', (data) => {
            var res = "";
            data.forEach((child) => {
                
                
                if(child.val().songID === uuid){
                    console.log(child.val());
                    res = child.val().lyric;

                    console.log(res);
                    return
                }
                
            })
            setLyric(res);
        })

    }

    useEffect(() => {
        readFunction()
    }, []);



    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollView}>
                <Text style={styles.lyrics}>
                    {lyric}
                </Text>
            </ScrollView>
        </SafeAreaView>
    )
}

export default SongDetail
