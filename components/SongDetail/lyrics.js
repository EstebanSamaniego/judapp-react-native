export default[{
    "1732525c-dd18-46d8-ba3e-203847b94dc0" : {
      "lyric" : "No prosperará el arma forjada\nla oscuridad no prevalecera\nporque el Dios que sirvo\nsiempre triunfará\nmi Dios no fallará,\nmi Dios no fallará\n\n[coro]\nvoy a ver la victoria\nvoy a ver la victoria\nla batalla es tuya, Señor\nvoy a ver la victoria\nvoy a ver la victoria\nla batalla es tuya, Señor\n\n[verso 2]\nhay poder en el nombre de Cristo\ncada guerra que El pelea ganará\nnunca huiré de los gigantes \nsé como terminará\nsé como terminará\n\n[coro]\nvoy a ver la victoria\nvoy a ver la victoria\nla batalla es tuya, Señor\nvoy a ver la victoria\nvoy a ver la victoria\nla batalla es tuya, señor.\n\n\n(puente)\ntodo lo que viene del enemigo\nlo transformas para bien \nlo transformas para bien \ntodo lo que viene del enemigo \nlo transformas para bien\nlo transformas para bien.\n[(alto)]\ntodo lo que viene del enemigo \nlo transformas para bien\nlo transformas para bien\ntodo lo que viene del enemigo\nlo transformas para bien\nlo transformas para bien.\n\n(coro)\nvoy a ver la victoria\nvoy a ver la victoria\nla batalla es tuya, Señor\nvoy a ver la victoria\nvoy a ver la victoria\nla batalla es tuya, señor.\n\n(Puente) alto.\nTodo lo que viene del enemigo\nlo transformas para bien \nlo transformas para bien.\nTodo lo que viene del enemigo\nlo transformas para bien \nlo transformas para bien.",
      "songID" : "e1e13d97-6372-4c6f-8f16-2c9125363a61",
      "uuid" : "1732525c-dd18-46d8-ba3e-203847b94dc0"
    },
    "18f35217-53b9-44af-9fc1-0ac4157c77bd" : {
      "lyric" : "Ante ti me acerco hoy\nMe presento tal y como soy\nMi pasado lleno de dolor\nTu Espíritu me levantó\n\nMuchas veces intenté y caí\nPero aún así seguiste ahí\nMe recuerdas que no soy mi error\nTú me llamas un hijo de Dios\nMi esperanza está en tu gran Amor\n\nCORO\nTu gracia triunfa sobre el juicio\nMás grande que el pecado\nTu amor no tiene fin\nTu bondad Dios me lleva a arrepentirme\nA tu corazón, tu corazón\nAnhelo Dios\n\nPareciera ser que un pecador\nNo pudiera recibir perdón\nPero al pródigo tu llamarás\nBienvenido a casa me dirás\n\n[CORO]\n\n// Solo a Cristo yo me rindo\nTodo se lo entrego a Él\nPara siempre en Él confío\nA su lado viviré //\n\n[CORO]",
      "songID" : "50a158ce-c591-422c-9396-f08a32950818",
      "uuid" : "18f35217-53b9-44af-9fc1-0ac4157c77bd"
    },
    "23a4b23b-51f0-464f-83bf-20fc350a3728" : {
      "lyric" : "//Eterno y sublime rey\nEl cielo no te puede contener\nHabitas en lo alto Dios\nLa tierra es el estrado de tus pies.//\n\n*Coro* \n¿Cómo construir una habitación\nUn mejor lugar en donde puedas descansar?\n¿Cómo construir una habitación\nQue sea un santuario para ti?\n\nSoberano creador\nEn luz inalcanzable tú estás\nFormaste todo con tu voz\nEl universo en tus manos está.\n\n*Coro*\nVamos a construir una habitación\nUn mejor lugar en donde puedas descansar\nVamos a construir una habitación\nSeremos un santuario para ti.",
      "songID" : "83f006cc-5439-43fe-85f9-642ee8cc58f8",
      "uuid" : "23a4b23b-51f0-464f-83bf-20fc350a3728"
    },
    "2600d08c-4893-4bf2-a5b6-9a4ca2f3f807" : {
      "lyric" : "Como el padre y tu Jesùs su hijo uno son\nHaznos uno.\nTu pasion Jesùs\nVemos unidos en amopr\nHaznos uno\n\n\n*Coro*\nPadre\nHaznos uno en ti\nEn espiritu y en amor\nUno.\n\nPadre\nHaznos uno en ti\nEn el cuerpo y en la fe\nHaznos uno.\n\nComo el padre y tu Jesùs su hijo uno son\nHaznos uno.\nTu pasion Jesùs\nVemos unidos\nEn amor\nHaznos uno\n\n *Coro*\nPadre\nHaznos uno en ti\nEn espiritu y en amor\nUno.\n\nPadre\nHaznos uno en ti\nEn el cuerpo y en la fe\nHaznos uno.\n\n",
      "songID" : "48eb9f36-8b98-4b85-9e74-5abd3cd19d37",
      "uuid" : "2600d08c-4893-4bf2-a5b6-9a4ca2f3f807"
    },
    "26bba68c-6fe1-40e9-98df-67df725a6b61" : {
      "lyric" : "Mi alma estaba\nRota y herida\nPero Tu gracia\nLa restauró\nManos vacías\nQue Tú llenaste\nSoy libre en Ti\nSoy libre en Ti\n\nSublime gracia del Señor\nQue a un pecador salvó\nFui ciego mas\nHoy veo yo\nPerdido y Él me halló\n\nAhora puedo ver\nPuedo ver sus ojos de amor\nQuebrantado fue\nPara darnos su salvación\n\nTú no me juzgas\nPor mis fracasos\nTú me aceptas\nTal como soy\nToma mi vida\nComo vasija\nPara mostrar\nTu gloria en mí\n\n//Sublime gracia del Señor\nQue a un pecador salvó\nFui ciego mas\nHoy veo yo\nPerdido y Él me halló\nAhora puedo ver\nPuedo ver sus ojos de amor\nQuebrantado fue\nPara darnos su salvación//",
      "songID" : "7a015672-6264-4a95-b7f1-8bff881acb4c",
      "uuid" : "26bba68c-6fe1-40e9-98df-67df725a6b61"
    },
    "29713bc6-f5db-436d-a34a-e04bb376d4ea" : {
      "lyric" : "[Verso 1]\nDesolados ya no más\nSomos herederos con Jesús\nExtranjeros ya no más\nAhora somos hijos por la Cruz\n\n[Coro]\nSomos iglesia, reino de luz\nNuestra bandera la Cruz de Jesús\nSomos iglesia, somos nación\nLa tumba vacía es nuestra canción\n\n[Verso 2]\nExtraviados ya no más\nNos llamas de vuelta a nuestro hogar\nSeparados ya no más\nSiempre a nuestro lado Tú estás\n\n[Coro]\nSomos iglesia, reino de luz\nNuestra bandera la Cruz de Jesús\nSomos iglesia, somos nación\nLa tumba vacía es nuestra canción\nSomos iglesia, reino de luz\nNuestra bandera la Cruz de Jesús\nSomos iglesia, somos nación\nLa tumba vacía es nuestra canción\nLa tumba vacía es nuestra canción!\n\n[Puente]\nVivo estas en gloria y honor\nReinas en victoria Señor\nNuestro ser entona a una vos\nCristo eres Dios, Cristo eres Dios\nVivo estas en gloria y honor\nReinas en victoria Señor\nNuestro ser entona a una vos\nCristo eres Dios, Cristo eres Dios\n\n[Coro]\nSomos iglesia, reino de luz\nNuestra bandera la Cruz de Jesús\nSomos iglesia, somos nación.\n\nLa tumba vacía es nuestra canción x3",
      "songID" : "6e94d85f-d7ef-40b0-a6a8-5c18cf3852c9",
      "uuid" : "29713bc6-f5db-436d-a34a-e04bb376d4ea"
    },
    "29961e2b-4e2f-4f9f-bf1d-50c1149eafe5" : {
      "lyric" : "Una estrella anunció\nQue en Belén Jesús nació\nPor amor Él descendió\nLa salvación con Él llegó\n\nAngelicales voces\nProclaman hoy su nombre\nRey de paz\nSalvador del mundo\n\nSiendo Dios se encarnó\nLa humanidad Él rescató\nSentado en su trono está\nJesús el Rey de majestad\n\nAngelicales voces\nProclaman hoy su nombre\nRey de paz\nSalvador del mundo\n\nSanto Santo Santo\nJesús te adoramos\nRey de paz\nSalvador del mundo\n\nVenid y adoremos\nVenid y adoremos\nVenid y adoremos\nA Cristo el Señor\n\nAngelicales voces\nProclaman hoy su nombre\nRey de paz\nSalvador del mundo\nRey de paz\nSalvador del mundo",
      "songID" : "593dcc4d-4c77-4a57-b3e2-5afcacde4b85",
      "uuid" : "29961e2b-4e2f-4f9f-bf1d-50c1149eafe5"
    },
    "2c747611-7558-482f-b49d-8344f2e410c1" : {
      "lyric" : "Digno es el Cordero Santo\nSanto Santo es Él\nLevantamos nuestra alabanza\nAl que en el trono esta.\n\n(coro)\nSanto, santo, santo Dios Todopoderoso\nQuien fue, quien es y quien vendrá\nLa creación hoy canta y damos gloria a Él\nTú eres digno por siempre y siempre.",
      "songID" : "73f96aea-00d3-49d7-81cd-7b6472f133b6",
      "uuid" : "2c747611-7558-482f-b49d-8344f2e410c1"
    },
    "33f8f9e4-61d4-4478-9f5e-8437209d38f8" : {
      "lyric" : "Me quiero acercar\nA tu lado estar\nQue el cielo sea real\nY la muerte negar\n\nQuiero oír las voces\nDe ángeles hoy\nCantando unidos…\n\nAleluya\nSanto, Santo\nPoderoso\nEl Gran Yo Soy\nAl que es digno\nIncomparable\nPoderoso\nEl Gran Yo Soy\n\nMe quiero acercar\nA tu lado estar\nAl mundo amar\nY lo oscuro odiar\n\nEl valle de huesos\nDe revivir\nCantando unidos…\n\nAleluya\nSanto, Santo\nPoderoso\nEl Gran Yo Soy\nAl que es digno\nIncomparable\nPoderoso\nEl Gran Yo Soy\n\nTiembla ante ti la tierra\nDemonios huyen ya\nAl mencionar tu nombre\nRey de majestad\nNo hay poder infernal\nQue pueda resistir\nAnte el poder y la presencia del…\n\n//Gran Yo Soy\nEl Gran Yo Soy\nGran Yo Soy//\n\nAleluya\nSanto, Santo\nPoderoso\nEl Gran Yo Soy\nAl que es digno\nIncomparable\nPoderoso\nEl Gran Yo Soy\n///El Gran Yo Soy///",
      "songID" : "08d43305-45b6-4a5b-b5f6-d9ed6d792f05",
      "uuid" : "33f8f9e4-61d4-4478-9f5e-8437209d38f8"
    },
    "37753754-2b62-4dc1-8a40-97eed76a40ac" : {
      "lyric" : "¡Oh, cuán dulce es fiar en Cristo, y entregarse todo a él; esperar en sus promesas, y en sus sendas serle fiel!\n\nCORO\nJesucristo, Jesucristo, ya tu amor probaste en mí; Jesucristo, Jesucristo, siempre quiero fiar en ti.\n\nEs muy dulce fiar en Cristo y cumplir su voluntad, no dudando su palabra, que es la luz y la verdad.\n\nSiempre es grato fiar en Cristo cuando busca el corazón, los tesoros celestiales de la paz y del perdón.\n\nSiempre en Ti confiar yo quiero mi precioso Salvador; en la Vida y en la muerte protección me de tu amor.",
      "songID" : "ebbf291f-b2a4-4f61-803d-8637ad514827",
      "uuid" : "37753754-2b62-4dc1-8a40-97eed76a40ac"
    },
    "3e351915-7902-4fea-b156-c498026d1144" : {
      "lyric" : "Quiero escuchar tu dulce voz\nRompiendo el silencio en mi ser\nSe que me haría estremecer\nMe haría llorar o reír\nY caería rendido ante Ti\n\nY no podría estar ante Ti\nEscuchándote hablar \nsin llorar como un niño\nY pasaría el tiempo así\nSin querer nada más\nNada más que escucharte hablar\n\nComo el ciervo busca por las aguas\nAsí clama mi alma, por ti Señor\nDía y noche yo tengo sed de Ti\nY sólo a ti buscaré\n\nLléname, lléname Señor\nDame más, más de Tu amor\nYo tengo sed sólo de Ti, Lléname Señor\n\nCerca de Ti yo quiero estar\nPara escuchar Tu voz y aprender de Ti\nQuiero ser un reflejo de Tu amor\nYo quiero vivir solo en Tu voluntad\n\nJesús, (Jesús) eres mi buen pastor\nTú conoces mi camino\nJesús, (Jesús) puedo confiar en Tí\nOh, mi Dios, me rindo hoy\nOh, mi Dios, me rindo hoy\n\nExaltate ¡Oh! Gran Cordero\nTú vives hoy y vivirás\nCoronate con mi alabanza\nTu nombre es el Vencedor\nTu nombre es el Vencedor\n\nOh, mi Dios, me rindo hoy",
      "songID" : "33e2a227-a073-43d2-900b-054ebd7f5c79",
      "uuid" : "3e351915-7902-4fea-b156-c498026d1144"
    },
    "421f1017-c9ab-4ce3-86a2-d3b035ca4687" : {
      "lyric" : "Me conoces \nen lo secreto\nme viste antes de nacer.\nMe acompañas \neres mi amigo fiel.\nNunca me dejas\nlo se muy bien.\n\nCORO\nOh Jesús, \n¿cómo pudo ser\nque viendo mi corazón\nme amaste tal como soy?\nEsa cruz, tu carta de amor\npara este pecador\neres tan bueno Dios.\n\nMe persigues\ncuando me alejo\nme traes a casa\nuna y otra vez.\nMe restauras\nestoy completo en Ti\nnunca me dejas \nlo se muy bien\nlo se muy bien.\n\ncoro.\n\nOhh oh ooh...\n\nAunque pase por el valle\nde la sombra y muerte\nya no temo, yo confío\ntú estás conmigo.\nY si caigo o me pierdo\nvienes a buscarme\nno me olvidas\ny me encuentras,\ny nunca te rindes.\n\nCORO 2\n//Tu amor inagotable es,\ntu bondad me persigue,\nte persigo.\n\nEn tu mesa me diste un lugar\nme recibes en casa\npara siempre//\n\nY si caigo o me pierdo\nvienes a buscarme\nno me olvidas\ny me encuentras,\ny nunca te rindes.",
      "songID" : "cde27280-1c62-4594-ac4a-2d5e37cb54fa",
      "uuid" : "421f1017-c9ab-4ce3-86a2-d3b035ca4687"
    },
    "7b961256-f7b5-4e2b-9378-92321c56a6a1" : {
      "lyric" : "[verso 1]\nTu sangre habla mejor\nQue todo el clamor\nQue en esta tierra hay\nJusticia para mi\nSé para mi favor\nJesùs tu sangre es.\n\n[coro]\nQué nos puede perdonar\nQué nos puede completar\nSólo de Jesús, sólo de Jesús\nLa sangre\n\nQué te da otro corazón\nY te hace amigo de Dios\nSólo de Jesús, sólo de Jesús\nLa sangre.\n\n[verso 2]\nTu cruz habla del amor\nDe su corazón\nUn camino nos abrió\nMe acerco con valor\nSin confianza terrenal\nJesús tu sangre es.\n\n[coro]\nQué nos puede perdonar\nQué nos puede completar\nSólo de Jesús, sólo de Jesús\nLa sangre\n\nQué te da otro corazón\nY te hace amigo de Dios\nSólo de Jesús, sólo de Jesús\nLa sangre.\n\n[puente]\n//Gracias por la cruz\nGracias por la cruz\nSólo de Jesús, sólo de Jesús\nLa sangre//\n\n[coro]\nQué nos puede perdonar\nQué nos puede completar\nSólo de Jesús, sólo de Jesús\nLa sangre\n\nQué te da otro corazón\nY te hace amigo de Dios\nSólo de Jesús, sólo de Jesús\nLa sangre",
      "songID" : "a98844cf-8960-4ea2-b9c2-8dd0cf680ec5",
      "uuid" : "7b961256-f7b5-4e2b-9378-92321c56a6a1"
    },
    "860b8063-bc9e-4201-a140-c99c6c5b97f9" : {
      "lyric" : "Oh, cantádmelas otra vez, bellas palabras de vida; hallo en ellas mi gozo y luz, bellas palabras de vida. Sí, de luz y vida, son sostén y guía.\n\nCORO\n¡Qué bellas son, qué bellas son! Bellas palabras de vida. ¡Qué bellas son, qué bellas son! Bellas palabras de vida.\n\nJesucristo a todos da bellas palabras de vida; oye su dulce voz, mortal, bellas palabras de vida. Bondadoso te salva, y al cielo te llama.\n\nGrato el cántico sonará, bellas palabras de vida; tus pecados perdonará, bellas palabras de vida. Sí, de luz y vida son sostén y guía.",
      "songID" : "538c4a43-4aea-48a5-b105-85f8a27f1041",
      "uuid" : "860b8063-bc9e-4201-a140-c99c6c5b97f9"
    },
    "9299be62-6241-4783-94ba-63ce4e900033" : {
      "lyric" : "[Verso 1]\nMuros rodeando estoy\nPensé que caerían hoy\nMas nunca me has fallado Dios\nLa espera terminará\nSé que has vencido ya\nNunca me has fallado Dios\n\n[Coro 1]\nEn ti confiaré\nTu promesa sigue en pié\nTú eres fiel\nConfiado andaré\nEn tus manos estaré\nSiempre has sido fiel\n\n[Verso 2]\nLa noche acabará\nTu Palabra se cumplirá\nMi corazón te alabará\nCristo, mi Salvador\nCúbreme con tu amor\nMi corazón te alabará\n\n[Coro 2]\nEn ti confiaré\nTu promesa sigue en pié\nTú eres fiel\nConfiado andaré\nEn tus manos estaré\nTú eres fiel\nEn ti confiaré\nTu promesa sigue en pié\nTú eres fiel\nConfiado andaré\nEn tus manos estaré\nSiempre has sido fiel\n\n[Puente]\nYo sé que tú, mueves montañas\nYo creo en ti, sé que lo harás otra vez\nAbriste el mar, en el desierto\nYo creo en ti, sé que lo harás otra vez\nYo sé que tú, mueves montañas\nYo creo en ti, sé que lo harás otra vez\nAbriste el mar, en el desierto\nYo creo en ti, sé que lo harás otra vez\n\n[Coro 3]\nEn ti confiaré\nTu promesa sigue en pié\nTú eres fiel\nConfiado andaré\nEn tus manos estaré\nSiempre has sido fiel\nEn tus manos estaré\nSiempre has sido fiel\nSiempre has sido fiel",
      "songID" : "e90ddf01-92b3-4358-9907-28b3182d9418",
      "uuid" : "9299be62-6241-4783-94ba-63ce4e900033"
    },
    "acadc6fa-320d-4dac-80d0-45e002f5be66" : {
      "lyric" : "Ya no lloren más\nla esperanza ya viene\nal mundo en dolor\nhe aquí el mesías.\n\nCanten ya los ángeles\nviene el cielo\nCristo nace en Belén\nviene el cielo.\n\nEl Amor venció\npecador eres libre\nadoremos hoy\nDios está con nosotros\n\nCanten ya los ángeles\nviene el cielo\nCristo nace en Belén\nviene el cielo,\nviene el cielo.\n\nSea aquí su gloria\ngloria en las alturas\npaz al mundo\nel salvador nació\n\nViene salvación\nsobre los quebrantados,\nal temor venció\npues Cristo es más grande,\nes más grande.\n\nCanten ya los ángeles\nviene el cielo\nCristo nace en Belén\nviene el cielo.\n\nCanten ya los ángeles\nviene el cielo\nla creación adora al Rey\nviene el cielo,\nviene el cielo.",
      "songID" : "5576c8e2-d1cc-44e0-a13c-27088b373411",
      "uuid" : "acadc6fa-320d-4dac-80d0-45e002f5be66"
    },
    "b452cc3c-fa85-4567-bcfa-c12767e30b51" : {
      "lyric" : "El esplendor de un Rey\nVestido en majestad\nLa tierra alegre está\nLa tierra alegre está\n\nCubierto está de luz\nVenció la oscuridad\nY tiembla a su voz\nY tiembla a su voz\n\nCuan grande es Dios\nCántale, cuan grande es Dios\nY todos lo verán\nCuan grande es Dios\n\nDía a día Él está\nY el tiempo está en Él\nPrincipio y el fin\nPrincipio y el fin\n\nLa Trinidad en Dios\nEl Padre, Hijo, Espíritu\nCordero y el León\nCordero y el León\n\nCuan grande es Dios\nCántale, cuan grande es Dios\nY todos lo verán\nCuan grande es Dios\n\nTu Nombre sobre todo es\nEres digno de alabar\nY mi ser dirá\nCuan grande es Dios\n\nCuan grande es Dios\nCántale, cuan grande es Dios\nY todos lo verán\nCuan grande es Dios\n",
      "songID" : "97818b37-7aed-4451-b63c-07fe5e109953",
      "uuid" : "b452cc3c-fa85-4567-bcfa-c12767e30b51"
    },
    "bcf8dc92-1f71-4514-a214-25826db9edb6" : {
      "lyric" : "Maestro, se encrespan las aguas, \ny ruge la tempestad,\nlos grandes abismos del cielo \nse llenan de obscuridad.\n¿No ves que aquí perecemos? \n¿puedes dormir así,\ncuando el mar agitado nos abre \nprofundo sepulcro aquí?\n\nCORO:\nLos vientos, las ondas oirán tu voz, \n\"¡sea la paz!:\nCalmas las iras del negro mar,\nlas luchas del alma las haces cesar,\ny así la barquilla do va el Señor\nhundirse no puede en el mar traidor.\nDoquier se cumple su voluntad:\n\"¡Sea la paz! ¡sea la paz!\"\ntu voz resuena en la inmensidad: \n\"¡Sea la paz!\"\n\nMaestro, mi ser angustiado \nte busca con ansiedad,\nde mi alma en los antros profundos \nse libra cruel tempestad;\npasa el pecado a torrentes \nsobre mi frágil ser,\ny perezco, perezco, Maestro, \n¡oh, quiéreme socorrer!\n\nMaestro, pasó la tormenta, \nlos vientos no rugen ya,\ny sobre el cristal de las aguas \nel sol resplandecerá.\nMaestro, prolonga esta calma, \nno me abandones más:\nCruzaré los abismos contigo, \ngozando bendita paz",
      "songID" : "779e2087-f61e-4312-ac5f-b5774977d54d",
      "uuid" : "bcf8dc92-1f71-4514-a214-25826db9edb6"
    },
    "cbb8905a-a69a-4c31-a017-a806f2c50116" : {
      "lyric" : "Hay poder en la sangre, \ndel calvario fluyó\nEl cordero de Dios por mi murió\nUna gran roca su tumba cubrió\nPero en el tercero Él se levantó\n\n¿Dónde está oh muerte tu aguijón?\nMi Salvador me resucitó\nJesús prometió Él regresará\nEn toda su gloria Él volverá\n\nY cuando yo vea el cielo abrir\nYo cantaré: Él viene por mí\n\nToda honra, toda gloria \nes para Ti Jesús\nMi pasión, mis afectos \ntuyos son Jesús\nToda honra, toda gloria \nes para Ti Jesús\nMi pasión, mis afectos \ntuyos son Jesús\n\nToda honra, toda gloria \nes para Ti Jesús\nMi pasión, mis afectos \ntuyos son Jesús\nToda honra, toda gloria \nes para Ti Jesús\nMi pasión, mis afectos \ntuyos son Jesús\n\nAleluya, aleluya\nAleluya, Él viene por mí\n\nAleluya, aleluya\nAleluya, Él viene por mí\n\nAleluya, aleluya\nAleluya, Él viene por mí",
      "songID" : "cc834a2f-ca88-400e-951d-7984bb31928a",
      "uuid" : "cbb8905a-a69a-4c31-a017-a806f2c50116"
    },
    "cbec798a-40fd-4264-bcb1-972b4e177552" : {
      "lyric" : "Es Tu palabra, renuevas mi ser Unges mi cabeza Mi destino puedo ver Aún en la sombra y en la oscuridad Tú has prometido, no me soltarás. \n\nLo confesaré, tus pactos hablaré Mi corazón cantará.\n\n(Coro)\nJehová es mi Pastor, nada me faltará Jehová es mi pastor y yo no temeré Jehová es mi pastor, nada me faltará Jehová es mi pastor y yo no temeré.\n\nYo confesaré, tus pactos hablaré \nMi corazón cantará Jehová es mi pastor, nada me faltará Jehová es mi pastor y yo no temeré Jehová es mi pastor, nada me faltará Jehová es mi pastor y yo no temeré",
      "songID" : "94772c33-408a-4db2-9228-d01f8a4605c4",
      "uuid" : "cbec798a-40fd-4264-bcb1-972b4e177552"
    },
    "d01ebdc5-c3aa-4e0a-8b81-cad9b950310b" : {
      "lyric" : "¿Quién rompe el poder del pecado?\nSu amor es fuerte y poderoso\nEl Rey de gloria, El Rey de majestad\n\nConmueve el mundo con su estruendo\nY nos asombra con maravillas\nEl Rey de gloria, El Rey de majestad\n\nGracia sublime es\nPerfecto es tu amor\nTomaste mi lugar\nCargaste tú mi cruz\n\nTu vida diste ahí\nY ahora libre soy\nJesús te adoro\nPor lo que hiciste en mi\n\nPusiste en orden todo el caos\nNos adoptaste como tus hijos\nEl Rey de gloria, El Rey de majestad\n\nEl que gobierna con su justicia\nY resplandece con su belleza\nEl Rey de gloria, El Rey de majestad\n\nGracia sublime es\nPerfecto es tu amor\nTomaste mi lugar\nCargaste tú mi cruz\n\nTu vida diste ahí\nY ahora libre soy\nJesús te adoro\nPor lo que hiciste en mi\n\nDigno es el Cordero de Dios\nDigno es el Rey que la muerte venció\n\nGracia sublime es\nPerfecto es tu amor\nTomaste mi lugar\nCargaste tú mi cruz\n\nTu vida diste ahí\nY ahora libre soy\nJesús te adoro\nPor lo que hiciste en mi",
      "songID" : "ac0209fe-40f5-4a44-a009-add1d5b88e1d",
      "uuid" : "d01ebdc5-c3aa-4e0a-8b81-cad9b950310b"
    },
    "d3a2619c-ef73-468f-8e15-6070809b3d9c" : {
      "lyric" : "Mis fallas se esconden \ndentro de tu gloria\ntodo se me opone\nmas tú eres por mí.\nCristo mi fuerza eres tú.\n\nTodo poder es de el\nquien fue y quien vendrá\ntodo poder es de Dios\naleluya\n\nOrdenas mis pasos, \nvas por delante.\nDios yo dependo de Ti.\nToda mi esperanza\nen ti se encuentra\nDios yo dependo de Ti.\n\nTodo poder es de el\nquien fue y quien vendrá\ntodo poder es de Dios\naleluya\n\nTodo poder es de el\nquien fue y quien vendrá\ntodo poder es de Dios\naleluya",
      "songID" : "9d7de461-9a27-4375-848f-0ddab3c65fde",
      "uuid" : "d3a2619c-ef73-468f-8e15-6070809b3d9c"
    },
    "ddb4c1b4-da67-408a-accb-9ee5c0eda015" : {
      "lyric" : "Me envuelves hoy con una canción,\nmelodía de tu amor\nMe das libertad en la adversidad,\nya no hay más temor\n\nYa no soy esclavo del temor\nYo soy hijo de Dios\nYa no soy esclavo del temor\nYo soy hijo de Dios\n\nAntes de nacer escogido fui\npor tu gran amor\nVolví a nacer, pertenezco a ti\nTu sangre fluye en mi\n\nYa no soy esclavo del temor\nYo soy hijo de Dios\nYa no soy esclavo del temor\nYo soy hijo de Dios\n\nOoh, oooh, ooh, oooh\n\nEstoy rodeado \npor los brazos del Padre\ny hoy proclamo un canto de libertad\nFuimos perdonados por tu Gracia\nSomos tus hijos amados\nNos has hecho libres\n\n/ Ooh, oooh, ooh, oooh /\n\nAbriste el mar para que yo camine\nTu amor ahogó todo el temor\nMe rescataste y hoy cantaré:\nYo soy hijo de Dios\n\nAbriste el mar para que yo camine\nTu amor ahogó todo el temor\nMe rescataste y hoy cantaré:\nYo soy hijo de Dios\n\n// Yo soy hijo de Dios //",
      "songID" : "3cb336c9-15e4-47ae-b33f-248bdbb946f6",
      "uuid" : "ddb4c1b4-da67-408a-accb-9ee5c0eda015"
    },
    "e02f47bc-acd3-437a-bf71-3703af5ec873" : {
      "lyric" : "[Verso]\nEn la prueba, en la espera\nVino nuevo harás\nAnte ti, yo, hoy me entrego\nUn camino abrirás\n\n[Pre-Coro]\nEn tus manos sé que puedo descansar\nAún si no entiendo en ti puedo confiar\n\n[Coro]\nRindo mi vida\nComo una ofrenda a ti\nHaz lo que quieras\nHacer tú de mí\nQué puedo entregarte\nSi todo viene de ti\nDios, vino nuevo haz de mí\n\n[Verso]\nEn la prueba, en la espera\nVino nuevo harás\nAnte ti, yo, hoy me entrego\nUn camino abrirás\nUn camino abrirás.\n\n[Coro]\nRindo mi vida\nComo una ofrenda a ti\nHaz lo que quieras\nHacer tú de mí\nQué puedo entregarte\nSi todo viene de ti\nDios, vino nuevo haz de mí\nDios, vino nuevo haz de mí\nDios, vino nuevo haz de mí\n\n[Puente]\nDonde hay vino nuevo\nHay nuevas fuerzas\nHay esperanza\nTu reino está aquí\nLo viejo entrego\nPues tu nuevo fuego arde en mí\nOh, arde en mí\nDonde hay vino nuevo\nHay nuevas fuerzas\nHay esperanza\nTu reino está aquí\nLo viejo entrego\nPues tu nuevo fuego arde en mí.\n\n[Coro]\nRindo mi vida\nComo una ofrenda a ti\nHaz lo que quieras\nHacer tú de mí\nOh, qué puedo entregarte\nSi todo viene de ti\nDios, vino nuevo haz de mí\nDios, vino nuevo haz de mí\nDios, vino nuevo haz de mí.",
      "songID" : "e7122cad-8149-4528-ad8f-cb5271ff4427",
      "uuid" : "e02f47bc-acd3-437a-bf71-3703af5ec873"
    },
    "e06bf4b5-9bd7-485e-b5e4-e2d71bc74e9e" : {
      "lyric" : "[verso 1]\nmuévete oh señor\nven y obra un milagro hoy\ntú mano moviéndose está\nsé que tú llegarás\na la tumba de cada Lázaro\ntú voz llamándome está\n\n[pre coro]\nsé que puedes hacerlo\nmi Dios hazlo otra vez\n\n[coro]\ntodo es posible\ntodo es posible en tí\nno has perdido una batalla\nno has perdido una batalla\ny se qué nunca me fallarás \n\n[verso 2]\ntodo se cumplirá\npor el poder de tu espíritu\ntú viento moviéndose está\nmis murallas caen hoy \nasí cómo las de Jericó\nderrumba todo en mi interior\n\n[puente]\nno has perdido una batalla\nno has perdido una batalla\nno has perdido una batalla\nnunca fallarás\n\n\n\n\n",
      "songID" : "69f31d37-80ad-4e5a-accb-1538d15717ca",
      "uuid" : "e06bf4b5-9bd7-485e-b5e4-e2d71bc74e9e"
    },
    "e51aa6b7-44fb-4155-80fd-4a365378f71e" : {
      "lyric" : "Dios me invita a entrar en su presencia\ny a convivir con mi dulce Salvador\nMiro su poder y gran magnificencia\nMi rodilla doblo en adoración\n\nAdorad, Adorad\na el Rey de las edades, Adorad\nA mi Dios, mi Señor\nExaltado en su trono, adorad\nDe rodillas adoradle\nCon mi voda le amaré y adoraré\n\nEn su mano está el poder que nos da vida\npor su voz toda creación apareció\nes el Dios que mi plegaria siempre escucha cuando humildes a él venimos a adorar\n\nAdorad, Adorad\na el Rey de las edades, Adorad\nA mi Dios, mi Señor\nExaltado en su trono, adorad\nDe rodillas adoradle\nCon mi voda le amaré y adoraré",
      "songID" : "99dc0f53-a64b-48c3-bd4e-ff4f00a12599",
      "uuid" : "e51aa6b7-44fb-4155-80fd-4a365378f71e"
    },
    "e60939bb-7b9c-4435-895d-90e6c59ef2d9" : {
      "lyric" : "Tú eres la luz\nQue brilló en las tinieblas\nAbrió mis ojos pude ver\nMi corazón adora tu hermosura\nEsperanza de vida eres tú\n\nCORO\nVine adorarte, vine a postrarme\nVine a decir que eres mi DIOS\nSolo tú eres grande, solo tú eres digno\nEres asombroso para mi\n\nTú eres el Rey\nGrandemente exaltado\nGlorioso por siempre Señor\nAl mundo que creaste humilde viviste\nY pobre te hiciste por Amor\n\nPUENTE\nNunca sabré cuanto costó ver mi maldad sobre esa cruz\n",
      "songID" : "b778474b-35e4-4e06-80a7-aa96186d7081",
      "uuid" : "e60939bb-7b9c-4435-895d-90e6c59ef2d9"
    },
    "ebc98fbf-33ba-47ba-bfc9-57278944a831" : {
      "lyric" : "Al estar en la presencia\nDe tu divinidad\nY al contemplar la hermosura\nDe tu santidad\nMi espíritu se alegra\nEn tu majestad\nTe adoro a ti\nTe adoro a ti\n\nCuando veo la grandeza\nDe tu dulce amor\nY compruebo la pureza\nDe tu corazón\nMi espíritu se alegra\nEn tu majestad\nTe adoro a ti\nTe adoro a ti\n\nY al estar aquí, delante de ti,\nte adoraré\nPostrado ante ti, \nmi corazón te adora, oh Dios\nY siempre quiero estar para adorar\nY contemplar tu santidad\nTe adoro a ti, Señor\nTe adoro a ti\n\nY al estar aquí, delante de ti, \nte adoraré\nPostrado ante ti, \nmi corazón te adora, oh Dios\nY siempre quiero estar para adorar\nY contemplar tu santidad\nTe adoro a ti, Señor\nTe adoro a ti",
      "songID" : "da02621f-ecc7-4173-aaf7-3597e1a4976c",
      "uuid" : "ebc98fbf-33ba-47ba-bfc9-57278944a831"
    },
    "f2afd8bc-8267-4b61-80a2-3e5c1f8709df" : {
      "lyric" : "quién quién quién como Jehová\nque con su poder el mar abrió\nquién quién quién como Jehová\nque con su poder el mar abrió\n\n\nquién quién quién como Jehová\nque con su poder el mar abrió\nquién quién quién como Jehová\nque con su poder el mar abrió\n\nquién quién quién como Jehová\nque con su poder el mar abrió\nquién quién quién como Jehová\nque con su poder el mar abrió\n\nquién quién quién como Jehová\nque con su poder el mar abrió\nquién quién quién como Jehová\nque con su poder el mar abrió\n\nquién quién quién como Jehová\nque con su poder el mar abrió\nquién quién quién como Jehová\nque con su poder el mar abrió\n\nquién quién quién como Jehová\nque con su poder el mar abrió\nquién quién quién como Jehová\nque con su poder el mar abrió\n\nquién quién quién como Jehová\nque con su poder el mar abrió\nquién quién quién como Jehová\nque con su poder el mar abrió",
      "songID" : "53df1c23-d622-4cda-a645-e451c1a52e6f",
      "uuid" : "f2afd8bc-8267-4b61-80a2-3e5c1f8709df"
    },
    "f5ff7c09-01ca-44dd-8f43-b1fb6d521401" : {
      "lyric" : "Canten del amor de Cristo,\nensalzad al Redentor.\nTributadle santos todos\ngrande gloria y loor.\n\nCORO\nCuando estemos en gloria,\nen presencia de nuestro Redentor,\na una voz la historia\ndiremos del gran Vencedor\n\nYa el triunfo es seguro\na las huestes del Señor.\n¡Oh, luchad con la mirada\npuesta en nuestro Protector!\n\nAlcen la bandera, hermanos,\nde la cruz, y caminad.\nDe victoria en victoria\nsiempre firmes avanzad.\n\nFirmes vamos en la lucha,\n¡oh, soldados de la fe!\nNuestro el triunfo, escuchemos\nlos clamores: ¡Gloria al Rey!\n",
      "songID" : "eb050214-d807-4ec2-af57-6871f01eddf1",
      "uuid" : "f5ff7c09-01ca-44dd-8f43-b1fb6d521401"
    },
    "f8311135-fc0a-4e53-917c-f6ad13eaa617" : {
      "lyric" : "A Tus pies arde mi corazón\nA Tus pies entrego lo que soy\nEse lugar de mi seguridad\nDonde nadie me puede señalar\n\nMe perdonaste, me acercaste \na Tu presencia\nMe levantaste, \nhoy me postro a adorarte\n\n// No hay lugar más alto, más grande\nQue estar a Tus pies, \nQue estar a Tus pies //\n\nA Tus pies arde mi corazón\nA Tus pies entrego lo que soy\nEse lugar de mi seguridad\nDonde nadie me puede señalar\n\nMe perdonaste, me acercaste \na Tu presencia\nMe levantaste, \nhoy me postro a adorarte\n\n// No hay lugar más alto, más grande\nQue estar a Tus pies, \nQue estar a Tus pies //\n\n//Y aquí permaneceré, postrado a Tus pies\nY aquí permaneceré a los pies de Cristo//\n\n//No hay lugar más alto, más grande\nQue estar a Tus pies, \nQue estar a Tus pies//",
      "songID" : "678dffa1-bc76-4d30-819f-80c623ebb575",
      "uuid" : "f8311135-fc0a-4e53-917c-f6ad13eaa617"
    }
  }

]