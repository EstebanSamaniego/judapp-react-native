import React, { useState, useEffect } from 'react';
import SongItem from '../SongItem';
import styles from './styles';
import { Appbar } from 'react-native-paper';
import { 
    FlatList, 
    View,
    ActivityIndicator,
    TextInput
} from 'react-native';


import filter from 'lodash.filter';
import db from '../../database/firebase';

const SongsList =({navigation}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [titulos, setTitulos] = useState([]);
    const [error, setError] = useState(null);
    const [query, setQuery] = useState('');
    const [fullData, setFullData] = useState([]);
    var formattedQuery;
    var filteredData;    

    const _handleSearch = () => {
        console.log("handleSearch");
    }

    const _handleMore = () => {
        console.log("handleMore");
    }
    
    const handleSearch = text => {
        formattedQuery = text.toLowerCase();
        filteredData = filter(fullData, song => {
            return contains(song, formattedQuery);
        });

        setTitulos(filteredData);
        setQuery(formattedQuery);
        
    };
    
    const contains = ({ artista, titulo, uuid }, query) => {
        const ft = titulo.toLowerCase();
        return ft.includes(query);
    };

    
    function renderHeader() {

        return (
          <View
            style={{
              backgroundColor: '#fff',
              padding: 10,
              marginVertical: 10,
              borderRadius: 20,
              marginRight: 10,
              marginLeft: 10
            }}
          >
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              clearButtonMode="always"
              defaultValue={query}
              onChangeText={queryText => handleSearch(queryText)}
              placeholder="Search"
              style={{ backgroundColor: '#fff', paddingHorizontal: 20 }}
            />
          </View>
        );

       
      }
      
    function readFunction(){
        db.ref('Song').once('value', (data) => {
            var main = [];
            data.forEach((child) => {
                main.push(child.val())
            })
            setTitulos(main);
            setFullData(main);
        })

        setIsLoading(false);

    }

    useEffect(() => {
        setIsLoading(true);
        readFunction();
    }, []);

  
    if (isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" color="#5500dc" />
            </View>
        );
    }else{
        return (
            <View style={styles.container}>
                <Appbar.Header style={styles.appBarStyle}>
                    <Appbar.Content title="Judapp"/>
                    <Appbar.Action icon="magnify" onPress={_handleSearch}/>
                    <Appbar.Action icon="dots-vertical" onPress={_handleMore}/>
                </Appbar.Header>
    
                <FlatList 
                    data={titulos}
                    renderItem={ 
                        ({item}) => <SongItem song={item} onPress={() => {navigation.push('SongDetail', {artista: item.artista, name: item.titulo, uuid: item.uuid})} }/>
                    }
                    keyExtractor={(item, index) => index.toString()}
                    ListHeaderComponent={renderHeader}
                />
            </View>
        )
    }    
}

export default SongsList

