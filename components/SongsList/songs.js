export default [{
    title: 'Shall not want',
    artist: 'Elevation Worship',
    lyrics: 'Will You be my light, when I cannot see When I can’t take another step, Lord, would You carry me',
},{
    title: 'Lo haras otra vez',
    artist: 'Elevation Worship',
    lyrics: 'Yo se que tu mueves montañas se que lo haras otra vez',
},{
    title: 'Mercy',
    artist: 'Elevation Worship',
    lyrics: 'Im the living proof of what the mercy of God can do',
}]