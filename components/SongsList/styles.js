import { Dimensions, StyleSheet } from 'react-native';

const styles =  StyleSheet.create({
    container: {
        width: '100%',
        height: Dimensions.get('window').height,
    },

    appBarStyle: {
        backgroundColor: '#a7391d'
    }
});

export default styles;