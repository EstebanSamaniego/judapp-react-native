import { Dimensions, StyleSheet } from 'react-native';

const styles =  StyleSheet.create({
    container: {
        width: '100%',
        padding: 20,
        borderWidth: 0.7,
        borderColor: '#B7B7B7',
    },
    songTitle: {
        fontSize: 20,
        fontWeight: '600',
        color: '#161616',
    },
    songArtist:{
        fontSize: 15,
        color: '#4A4A4A',
    }
});

export default styles;