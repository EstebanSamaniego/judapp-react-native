import React from 'react'
import { Text, View, Pressable } from 'react-native'

import styles from './styles';


const SongItem = (props) => {

    
    const { artista, titulo, uuid } = props.song;
    const  onPress  = props.onPress;

    return (
        <View style={styles.container}>
            <Pressable style={styles.songTitle}
                onPress={() => onPress() }
            >
                <Text style={styles.songTitle}>{titulo}</Text>
                <Text style={styles.songArtist}>{artista}</Text>
                
            </Pressable>
        </View>
    )
}

export default SongItem
