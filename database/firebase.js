import firebase from 'firebase'

var firebaseConfig = {
  apiKey: "AIzaSyBRLlABb4JCcBgdlnWmqY3fPMhQT3Bw-bc",
  authDomain: "fir-project-f390b.firebaseapp.com",
  databaseURL: "https://fir-project-f390b.firebaseio.com",
  projectId: "fir-project-f390b",
  storageBucket: "fir-project-f390b.appspot.com",
  messagingSenderId: "467503274674",
  appId: "1:467503274674:web:1ff0463666f2abe8"
};

firebase.initializeApp(firebaseConfig);

const db = firebase.database();

export default db;